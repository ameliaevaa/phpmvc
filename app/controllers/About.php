<?php
class About extends Controller {
    public function index($nama = "Eva Amelia", $umur = 16) {
        $data['nama'] = $nama;
        $data['umur'] = $umur;
        $data['title'] = "Halaman About";
        $this->view('templates/header',$data);
        $this->view('about/index', $data);
        $this->view('templates/footer');
    }

    public function page() {
        $data['title'] = "Halaman Page";
        $this->view('templates/header',$data);
        $this->view('about/page');
        $this->view('templates/footer');
    }
}